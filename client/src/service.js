import axios from 'axios';

const prefix = 'http://localhost';

export function query(param) {
    const path = param.path;
    delete param.path;
    return axios({
        url: prefix + path,
        method: 'get',
        params: param,
        headers: {
            'Authorization':localStorage.getItem('token')
        }
    })
    //     .then(function (response) {
    //     return response;
    // })
}
export function create(param) {
    const path = param.path;
    delete param.path;
    return axios({
        url: prefix + path,
        method: 'post',
        data: param
    })
}
