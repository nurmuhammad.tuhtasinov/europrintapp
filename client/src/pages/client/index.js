import React, {Component} from 'react';
import axios from 'axios';
import {PATH_PREFIX} from "../../utils";
import Pagination from "react-js-pagination";

class Index extends Component {

    componentDidMount() {
        axios({
            url: PATH_PREFIX + '/api/client',
            method: 'get',
            params: {
                search: '',
                page: 1,
                size: 2
            },
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        }).then(function (response) {
            this.setState({
                data: response.data.object.content
            })
        }.bind(this));
    }

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            activePage: 1,
            totalItems: 100,
            size: 10
        }
    }


    render() {
        const {user} = this.props;
        const {data, activePage, totalItems, size} = this.state;
        return (
            <div>
                <div className="row">
                    <div className="col-md-10 offset-1 text-center">
                        <table className="table my-3">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Full name</th>
                                <th>Phone number</th>
                                <th>Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                data.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{item.fullName}</td>
                                            <td>{item.phoneNumber}</td>
                                            <td>
                                                <button className={"btn btn-primary"}>Edit</button>
                                                {
                                                    user && user.roles.filter(item => item.roleName === 'ROLE_DIRECTOR').length === 0 ? '' :
                                                        <button className={"btn btn-danger"}>Delete</button>
                                                }
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                            <Pagination
                                activePage={activePage}
                                itemsCountPerPage={size}
                                totalItemsCount={totalItems}
                                nextPageText={">"}
                                prevPageText={"<"}
                                // pageRangeDisplayed={45}
                                // onChange={this.handlePageChange.bind(this)}
                            />
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;