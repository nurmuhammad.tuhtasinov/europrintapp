import React, {Component} from 'react';
import axios from "axios";
import {PATH_PREFIX} from "../../utils";

class Index extends Component {
    render() {
        const handleLogin=(event)=>{
            event.preventDefault(); // prevent from F5
            const username = event.target[0].value;
            const password = event.target[1].value;

            return axios({
                url: PATH_PREFIX + '/api/auth',
                method: 'post',
                data: {
                    username,
                    password,
                },
                // headers: {
                //     'Authorization':localStorage.getItem('token')
                // }
            }).then(function (response) {
                localStorage.setItem('token',response.data.tokenType + " " + response.data.accessToken);
                window.location.reload(true);
            })
        };

        return (
            <div>
                <div className="container">
                    <div className="row mt-5">
                        <div className="col-md-4 offset-4">
                            <div className="card">
                                <div className="card-header bg-primary text-white">
                                    <h3>Login</h3>
                                </div>
                                <div className="card-body ">
                                    <form onSubmit={handleLogin}>
                                        <input type="text" className={"form-control mt-2"} placeholder={"username"}/>
                                        <input type="password" className={"form-control mt-2"} placeholder={"password"}/>
                                        <button className={"btn btn-success mt-2"} type={"submit"}>Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;