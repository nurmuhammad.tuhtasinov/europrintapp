import React, {Component} from 'react';
import axios from 'axios';
import {PATH_PREFIX} from "../../utils";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import OrderModal from '../../components/OrderModal'
import {generateForm} from "../../components/GenerateOrderForm";

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            modalVisibility: false,
            currentMessage: '',
            orderModalVisible: false,
            inputRow: [0]
        }
    }

    componentDidMount() {
        axios({
            url: PATH_PREFIX + '/api/message',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(function (response) {
            this.setState({
                data: response.data.object
            });
        }.bind(this))
    }


    render() {
        const {data, modalVisibility, currentMessage, orderModalVisible, inputRow} = this.state;

        const openModal = (item) => {
            this.setState({
                modalVisibility: true,
                currentMessage: item
            });


            // axios({
            //     url:PATH_PREFIX+'/api/message/'+item.id,
            //     method:'patch',
            //     params:{
            //         state:"INACTIVE"
            //     },
            //     headers: {
            //         'Authorization':localStorage.getItem('token')
            //     }
            // })
        };


        const closeModal = () => {
            axios({
                url: PATH_PREFIX + '/api/message/' + currentMessage.id,
                method: 'patch',
                params: {
                    state: "ACTIVE"
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            });
            this.setState({
                modalVisibility: false,
                currentMessage: ''
            });
        };
        const saveOrder = (event, errors, values) => {
            axios({
                url:PATH_PREFIX+'/api/order',
                method:'post',
                data:{
                    products: generateForm(values),
                    isBot:true,
                    clientId:currentMessage.client.id
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            })
        }

        const ignoreMessage = () => {

        }

        const changeMessageStatus = (status) => {
            axios({
                url: PATH_PREFIX + '/api/message/' + currentMessage.id,
                method: 'patch',
                params: {
                    state: status
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            })
        };

        const openOrderModal = () => {
            this.setState({
                orderModalVisible: true,
                modalVisibility: false
            })
        };
        const closeOrderModal = () => {
            this.setState({
                orderModalVisible: false,
                modalVisibility: true,
                inputRow: [0]
            })
        }

        const addProduct = () => {
            if (inputRow.length === 1) {
                inputRow.push(1);
            } else {
                let newRow = inputRow[inputRow.length - 1] + 1;
                inputRow.push(newRow);
            }
            this.setState({
                inputRow
            })
        };
        const deleteProduct = (item) => {
            inputRow.splice(item, 1);
            this.setState({
                inputRow
            })
        }

        return (
            <div>
                <div className="container">
                    <OrderModal
                         addProduct={addProduct} closeOrderModal={closeOrderModal} deleteProduct={deleteProduct} inputRow={inputRow} orderModalVisible={orderModalVisible} saveOrder={saveOrder}
                    />
                    <Modal className={"openModal"} isOpen={modalVisibility}>
                        <ModalHeader>
                            <p>
                                {
                                    currentMessage && currentMessage.client.phoneNumber
                                }
                            </p>
                            {'\n'}
                            {
                                currentMessage && currentMessage.client.fullName
                            }
                        </ModalHeader>
                        <ModalBody>
                            <div className="row">
                                <div className="col-md-8">
                                    <div className={'clientMessage'}>
                                        {
                                            currentMessage && currentMessage.text
                                        }
                                    </div>
                                </div>
                                <div className="col-md-4 vert">
                                    <button onClick={openOrderModal} className={'my-button'}> ORDER</button>
                                </div>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <div className="row">
                                <button onClick={() => changeMessageStatus("PRICING")}
                                        className='btn btn-dark text-white mx-1'>Pricing
                                </button>
                                {' '}
                                <button onClick={() => changeMessageStatus("DESIGNING")}
                                        className='btn btn-dark text-white mx-1'>Designing
                                </button>
                                {' '}
                                <button onClick={() => changeMessageStatus("CONSULTING")}
                                        className='btn btn-dark text-white mx-1'>Consulting
                                </button>
                                {' '}
                                <button onClick={() => changeMessageStatus("IGNORED")}
                                        className='btn btn-danger text-white mx-1'>Ignoring
                                </button>
                                {' '}
                                <button onClick={closeModal} className='btn btn-outline-danger text-black mx-1'>Close
                                </button>
                                {' '}
                            </div>
                            {/*<div className="row">*/}
                            {/*    <Button color="success" onClick={saveOrder}>Create order</Button>{' '}*/}
                            {/*    <Button color="secondary" onClick={closeModal}>Cancel</Button>*/}
                            {/*</div>*/}
                        </ModalFooter>
                    </Modal>
                    <div className="row">
                        <div className="col-md">
                            <table className="table">
                                <thead>
                                <tr>
                                    <th>Active</th>
                                    <th>Pricing</th>
                                    <th>Designing</th>
                                    <th>Consulting</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    data && data.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>
                                                    <div onClick={() => openModal(item)}
                                                         className="message card border-dark">

                                                        <div className="card-body">

                                                            {
                                                                item.text
                                                            }
                                                        </div>
                                                        {/*<div className="card-footer">*/}
                                                        {/*<button className={"btn btn-dark text-white float-right"} onClick={ignoreMessage}>Ignore</button>*/}
                                                        {/*</div>*/}
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;