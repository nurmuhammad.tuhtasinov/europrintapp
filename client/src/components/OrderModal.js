import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvInput} from "availity-reactstrap-validation";

const OrderModal = ({
                        orderModalVisible,
                        saveOrder,
                        inputRow,
                        addProduct,
                        deleteProduct,
                        closeOrderModal
                    }) => {


        return (

            <Modal className={"orderModal"} isOpen={orderModalVisible}>
                <ModalHeader>
                    Order Modal
                </ModalHeader>
                <ModalBody>

                    <AvForm onSubmit={saveOrder} id={"form"}>
                        <table className='table table-striped'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>3D Size</th>
                                <th>Material</th>
                                <th>Knife</th>
                                <th>Process</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Amount</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>

                            {
                                inputRow && inputRow.map((item, index) => {
                                    return (
                                        <tr key={item}>
                                            <td>{index + 1}</td>
                                            <td><AvInput name={"product/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"product"}/></td>
                                            <td><AvInput name={"threeDSize/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"3D Size"}/></td>
                                            <td><AvInput name={"material/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"material"}/></td>
                                            <td><AvInput name={"knife/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"knife"}/></td>
                                            <td><AvInput name={"process/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"process"}/></td>
                                            <td><AvInput name={"type/" + index} type="text"
                                                         className={"form-control mt-2"}
                                                         placeholder={"type"}/></td>
                                            <td><AvInput name={"price/" + index} type="number"
                                                         className={"form-control mt-2"}
                                                         placeholder={"price"}/></td>
                                            <td><AvInput name={"amount/" + index} type="number"
                                                         className={"form-control mt-2"}
                                                         placeholder={"count"}/></td>
                                            <td>{index === 0 ? '' : <div className="col mt-2">
                                                <button onClick={() => deleteProduct(index)}
                                                        className='btn btn-danger'>-
                                                </button>
                                            </div>}</td>
                                        </tr>
                                    )
                                })

                            }
                            </tbody>
                        </table>


                        <button type="button" onClick={addProduct} className={"btn btn-success mt-2"}>Add
                            product
                        </button>

                    </AvForm>
                </ModalBody>
                <ModalFooter>

                    <button form={"form"} className={"btn btn-success mt-2"}>Create order</button>
                    <Button color="secondary" onClick={closeOrderModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );



}

export default OrderModal;