export function generateForm(values) {
    let arr=[];
    let keys = Object.keys(values);
    let rowCount = keys.length / 8;
    for (let i = 0; i < rowCount; i++) {
        let a = {};
        keys.map(function (item, index) {
            if (item.split('/')[1] == i) {
                a[item.split('/')[0]] = values[item]
            }
        })
        arr.push(a);
    }
    return arr;
}