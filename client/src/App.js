import React, {Component} from 'react';
import './App.css';
import {Router, Switch, Route, Link} from "react-router-dom";
import Home from './pages/home/index';
import Message from './pages/message/index';
import Order from './pages/order/index';
import Manager from './pages/manager/index';
import Report from './pages/report/index';
import Client from './pages/client/index';
import Login from './pages/login/index';
import history from "./history";
import 'bootstrap/dist/css/bootstrap.min.css';
import {query} from "./service";

class App extends Component {
    async componentDidMount() {
        const {data} = await query({
            path: '/api/user/me'
        });
        if (!data.success) {
            history.push("/login");
            this.setState({
                redirect: true,
            });
        } else {
            if (localStorage.getItem('currentPage')===null){
                history.push('/home');
            }else {
                history.push(localStorage.getItem('currentPage'));
            }
            this.setState({
                redirect: false,
                user: data.object,
            });
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            user: '',
        }
    }

    render() {
        const {redirect,user} = this.state;
        const handlePage=(param)=>{
            localStorage.setItem('currentPage',param);
        }
        return (
            <div className={"container-fluid"}>
                <Router history={history}>
                    <div className="row">
                        {!redirect ?
                            <div className={"col-md-2 sider"}>
                                <ul className="list-group">
                                    <li className={"list-group-item border-bottom font-weight-bold text-center"}>
                                        Euro print
                                    </li>
                                    <Link to={"/home"} onClick={()=>handlePage('/home')}>
                                        <li className={"list-group-item list-group-item-action"}> Home</li>
                                    </Link>
                                    <Link to={"/message"} onClick={()=>handlePage('/message')}>
                                        <li className={"list-group-item list-group-item-action"}> Message</li>
                                    </Link>
                                    <Link to={"/order"} onClick={()=>handlePage('/order')}>
                                        <li className={"list-group-item list-group-item-action"}> Order</li>
                                    </Link>
                                    {
                                        user&&user.roles.filter(item=> item.roleName === 'ROLE_DIRECTOR').length===0 ?'':
                                        <Link to={"/manager"} onClick={()=>handlePage('/manager')}>
                                            <li className={"list-group-item list-group-item-action"}> Managers</li>
                                        </Link>
                                    }
                                    <Link to={"/client"} onClick={()=>handlePage('/client')}>
                                        <li className={"list-group-item list-group-item-action"}> Clients</li>
                                    </Link>
                                    <Link to={"/report"} onClick={()=>handlePage('/report')}>
                                        <li className={"list-group-item list-group-item-action"}> Report</li>
                                    </Link>
                                </ul>
                            </div>
                            : ''
                        }

                        <div className="col">

                            <Switch location={history.location}>
                                <Route path="/login">
                                    <Login/>
                                </Route>
                            </Switch>

                            <Switch>
                                <Route path="/login">
                                </Route>
                                <Route path="/home">
                                    <Home/>
                                </Route>
                                <Route path="/order">
                                    <Order/>
                                </Route>
                                <Route path="/message">
                                    <Message/>
                                </Route>
                                <Route path="/manager">
                                    <Manager/>
                                </Route>
                                <Route path="/client">
                                    <Client user={user}/>
                                </Route>
                                <Route path="/report">
                                    <Report/>
                                </Route>
                            </Switch>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;

