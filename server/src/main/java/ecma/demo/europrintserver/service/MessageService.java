package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.MessageRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class MessageService {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    MessageRepository messageRepository;

    public HttpEntity<?> getAll(MessageStatus messageStatus) {
        return ResponseEntity.ok(new ApiResponse("success", true,
                messageRepository.findAllByMessageStatusOrderByCreatedAtAsc(MessageStatus.ACTIVE)));
    }

    public Message generateMessageForNewOrder(String description, Long chatId) {
        Optional<Client> client = clientRepository.findByTgId(chatId);
        if (client.isPresent()) {
            return messageRepository.save(new Message(
                    client.get(),
                    description,
                    null,
                    null,
                    MessageStatus.ACTIVE
            ));
        }
        return null;
    }

    public Message generateMessageForOldOrder(String description, Long chatId, UUID orderId) {
        Optional<Client> client = clientRepository.findByTgId(chatId);
        Order order = orderRepository.findById(orderId).get();
        if (client.isPresent()) {
            return new Message(
                    client.get(),
                    description,
                    null,
                    order,
                    MessageStatus.ACTIVE
            );
        }
        return null;
    }

    public HttpEntity<?> update(UUID id, String state) {
        Message message = messageRepository.findById(id).get();
        message.setMessageStatus(MessageStatus.valueOf(state));
        messageRepository.save(message);
        return ResponseEntity.ok(new ApiResponse("success",true));
    }
}
