package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqClient;
import ecma.demo.europrintserver.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public HttpEntity<?> get(String search, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Client> clients = clientRepository.findAllByFullNameContainingIgnoreCaseOrPhoneNumberContainingOrderByCreatedAtDesc(pageable, search, search);
        return ResponseEntity.ok(new ApiResponse("success",true,clients));
    }


    public HttpEntity<?> save(ReqClient reqClient) {
        clientRepository.save(new Client(reqClient.getFullname(),reqClient.getPhoneNumber()));
        return ResponseEntity.ok(new ApiResponse("success",true));
    }


    public HttpEntity<?> edit(UUID id, ReqClient reqClient) {
        Client client = clientRepository.findById(id).get();
        client.setFullName(reqClient.getFullname());
        client.setPhoneNumber(reqClient.getPhoneNumber());
        clientRepository.save(client);
        return ResponseEntity.ok(new ApiResponse("succcess",true));
    }

    public HttpEntity<?> delete(UUID id) {
        clientRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse("success",true));
    }
}
