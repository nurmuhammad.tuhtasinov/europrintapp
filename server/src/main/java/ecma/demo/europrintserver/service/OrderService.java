package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.Product;
import ecma.demo.europrintserver.entity.enums.OrderPayStatus;
import ecma.demo.europrintserver.entity.enums.OrderStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqOrder;
import ecma.demo.europrintserver.payload.ReqProduct;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import ecma.demo.europrintserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ProductRepository productRepository;


    public HttpEntity<?> save(ReqOrder reqOrder) {
        Client client = clientRepository.findById(reqOrder.getClientId()).get();

        //orderStatusni tekshiraman.
        //orderPayStatusni tekshiraman.
        Order order = orderRepository.save(
                new Order(null, OrderStatus.PENDING, null,
                        OrderPayStatus.NOT_PAID, client, reqOrder.getIsBot()));

        for (ReqProduct orderProduct : reqOrder.getProducts()) {
            productRepository.save(new Product(orderProduct.getPrice(),
                                                orderProduct.getAmount(),
                                                order,
                                                orderProduct.getProduct(),
                                                orderProduct.getThreeDSize(),
                                                orderProduct.getMaterial(),
                                                orderProduct.getKnife(),
                                                orderProduct.getProcess(),
                                                orderProduct.getType()));
        }


        return ResponseEntity.ok(new ApiResponse("success",true));
    }
}
