package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqProduct {

    private Double price;

    private Integer amount;

    private String product;

    private String threeDSize;

    private String material;

    private String knife;

    private String process;

    private String type;

}
