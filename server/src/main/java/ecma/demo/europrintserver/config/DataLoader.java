package ecma.demo.europrintserver.config;

import ecma.demo.europrintserver.entity.Role;
import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.entity.enums.RoleName;
import ecma.demo.europrintserver.repository.RoleRepository;
import ecma.demo.europrintserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(new User(
                    "Nurmuhammad",
                    "Tuhtasinov",
                    "+998995119917",
                    "AB",
                    "1111111",
                    "Eagle4488",
                    passwordEncoder.encode("root123"),
                    roleRepository.saveAll(
                            new ArrayList<>(Arrays.asList(
                                    new Role(1, RoleName.ROLE_ADMIN,"desc"),
                                    new Role(2,RoleName.ROLE_DIRECTOR,"desc"),
                                    new Role(3,RoleName.ROLE_MANAGER,"desc"))
            ))));
        }
    }
}
