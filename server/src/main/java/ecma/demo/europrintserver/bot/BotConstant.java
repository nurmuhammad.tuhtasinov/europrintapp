package ecma.demo.europrintserver.bot;

public interface BotConstant {

    String BACK_TO_MAIN = "BACK_TO_MAIN";
    String GET_BALANCE = "GET_BALANCE";
    String MY_ORDERS = "MY_ORDERS";
    String SETTINGS = "SETTINGS";
    String NEW_ORDER_CLICKED = "NEW_ORDER_CLICKED";

}
