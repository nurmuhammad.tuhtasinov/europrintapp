package ecma.demo.europrintserver.bot.service;

import ecma.demo.europrintserver.bot.BotConstant;
import ecma.demo.europrintserver.payload.ReqOneRowButtons;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
public class ButtonService {

    public InlineKeyboardMarkup homeButton(){

        InlineKeyboardMarkup inlineKeyboardMarkup=new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> lists = new ArrayList<>();
        List<InlineKeyboardButton> list = new ArrayList<>();
        List<InlineKeyboardButton> list1 = new ArrayList<>();

        InlineKeyboardButton buttonBalance=new InlineKeyboardButton();
        InlineKeyboardButton buttonOrder=new InlineKeyboardButton();
        InlineKeyboardButton buttonSetting=new InlineKeyboardButton();


        buttonBalance.setText("Balance");
        buttonBalance.setCallbackData(BotConstant.GET_BALANCE);

        buttonOrder.setText("Orders");
        buttonOrder.setCallbackData(BotConstant.MY_ORDERS);

        buttonSetting.setText("Setting");
        buttonSetting.setCallbackData(BotConstant.SETTINGS);

        list.add(buttonBalance);
        list1.add(buttonOrder);
        list1.add(buttonSetting);
        lists.add(list);
        lists.add(list1);
        return inlineKeyboardMarkup.setKeyboard(lists);
    }

    public ReplyKeyboardMarkup contactButton(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton button = new KeyboardButton();
        button.setText("Raqamingizni junating");
        button.setRequestContact(true);
        row.add(button);
        rows.add(row);
        return replyKeyboardMarkup.setKeyboard(rows);
    }

    public InlineKeyboardMarkup myButton(String text,String query){
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows1 = new ArrayList<>();
        List<InlineKeyboardButton> buttonsList=  new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(query);
        buttonsList.add(button);
        rows1.add(buttonsList);
        inlineKeyboardMarkup.setKeyboard(rows1);
        return inlineKeyboardMarkup;
    }

    public InlineKeyboardMarkup oneRowButtons(List<ReqOneRowButtons> reqOneRowButtons){
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows1 = new ArrayList<>();
        List<InlineKeyboardButton> buttonsList=  new ArrayList<>();
        for (ReqOneRowButtons reqOneRowButton : reqOneRowButtons) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(reqOneRowButton.getText());
            button.setCallbackData(reqOneRowButton.getQuery());
            buttonsList.add(button);
        }
        rows1.add(buttonsList);
        inlineKeyboardMarkup.setKeyboard(rows1);
        return inlineKeyboardMarkup;
    }
}
