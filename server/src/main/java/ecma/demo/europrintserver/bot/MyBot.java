package ecma.demo.europrintserver.bot;

import ecma.demo.europrintserver.bot.service.ButtonService;
import ecma.demo.europrintserver.bot.service.TgService;
import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.TelegramState;
import ecma.demo.europrintserver.entity.TgMessage;
import ecma.demo.europrintserver.payload.ReqOneRowButtons;
import ecma.demo.europrintserver.repository.*;
//import ecma.demo.europrintserver.service.ClientService;
import ecma.demo.europrintserver.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class MyBot extends TelegramLongPollingBot {

    @Autowired
    ButtonService myButtons;
    @Autowired
    TgMessageRepository tgMessageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    TelegramStateRepository telegramStateRepository;
    @Autowired
    TgService tgService;
//    @Autowired
//    ClientService clientService;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    MessageService messageService;
    @Autowired
//    SimpMessageSendingOperations simpMessageSendingOperations;
//    @Autowired
//    ChatRepository chatRepository;


    @Override
    public String getBotToken() {
        return "960841040:AAG_F6iKAgzhiHH019Ng2GHEZGRDfMX8nAU";
    }

    @Override
    public String getBotUsername() {
        return "komil_jeynov_bot";
    }

    @Override
    public void onUpdateReceived(Update req) {

        if (req.hasMessage()) {
            Long chatId = req.getMessage().getChatId();

            if (req.getMessage().hasText()) {
                String text = req.getMessage().getText();

                if (text.equals("/start")) {
                    Optional<Client> oClient = clientRepository.findByTgId(chatId);
                    if (oClient.isPresent()) {
                        tgService.changeState(chatId, BotState.REGISTERED);
                        send(tgService.homePage(chatId));
                    } else {
                        SendMessage sendMessage = new SendMessage();
                        sendMessage.setChatId(chatId);
                        sendMessage.setText("Assalamu alaykum. Iltimos botdan to'liq foydalanish uchun raqamingizni jo'nating ");
                        sendMessage.setReplyMarkup(myButtons.contactButton());
                        tgService.changeState(chatId, BotState.FIRST_STATE);
                        send(sendMessage);
                    }
                    deleteClientMessage(req);
                } else {
                    Optional<TelegramState> byTgId = telegramStateRepository.findByTgId(chatId);
                    if (byTgId.isPresent()) {
                        String myState = byTgId.get().getTgState();
                        if (myState.equals(BotState.FIRST_STATE)) {
                            tgService.onOutRule(req, "Iltimos Share Contact tugmasini bosing");
                        } else if (myState.equals(BotState.REGISTERED)) {
                            tgService.onOutRule(req, "Iltimos yuqoridagilardan birini tanlang");
                        } else if (myState.equals(BotState.SHOWED_BALANCE)) {
                            tgService.onOutRule(req, "Iltimos bosh menyuga qaytish tugmasini bosing");
                        } else if (myState.equals(BotState.SHOWED_CLIENT_ORDERS)) {
                            tgService.onOutRule(req, "Iltimos oldingi buyurtmalardan birini tanlang yoki yangi buyurtma bering");
                        } else if (myState.equals(BotState.NEW_ORDER_DESC_ENTERING_FINISHED)) {
                            tgService.onOutRule(req, "Iltimos bosh menyuga qaytish tugmasini bosing");
                        } else if (myState.equals(BotState.NEW_ORDER_DESC_ENTERING)) {
                            delete(chatId);
                            deleteClientMessage(req);
                            SendMessage sendMessage = new SendMessage();
                            sendMessage.setText("So'rovingiz qabul qilindi. Sizga tez orada qo'ng'iroq qilishadi");
                            sendMessage.setChatId(chatId);
                            sendMessage.setReplyMarkup(myButtons.myButton("Bosh menuga qaytish", BotConstant.BACK_TO_MAIN));
                            send(sendMessage);
                            Message message = messageService.generateMessageForNewOrder(text, chatId);
//                            simpMessageSendingOperations.convertAndSend("/chat/all", message);
                            tgService.changeState(chatId, BotState.NEW_ORDER_DESC_ENTERING_FINISHED);
                        }
                    }
                }
            }
            if (req.getMessage().hasContact()) {
                String phoneNumber = req.getMessage().getContact().getPhoneNumber();
                clientRepository.save(new Client(null,phoneNumber,null,chatId));
                delete(req.getMessage().getChatId());
                deleteClientMessage(req);
                send(tgService.homePage(chatId));
            }
        }


        if (req.hasCallbackQuery()) {

            Long chatId = req.getCallbackQuery().getMessage().getChatId();
            String data = req.getCallbackQuery().getData();

            if (data.equals(BotConstant.GET_BALANCE)) {
                delete(chatId);
                SendMessage sendMessage = new SendMessage();
//                sendMessage.setText("<b>" + clientService.getClientBalanceForBot(chatId.intValue()) + "</b>");
                sendMessage.setChatId(chatId);
                sendMessage.setReplyMarkup(myButtons.myButton("\uD83C\uDFE0", BotConstant.BACK_TO_MAIN));
                send(sendMessage);
                tgService.changeState(chatId, BotState.SHOWED_BALANCE);
            } else if (data.equals(BotConstant.BACK_TO_MAIN)) {
                send(tgService.homePage(chatId));
            } else if (data.equals(BotConstant.MY_ORDERS)) {
                tgService.sendClientOrders(chatId);
                SendMessage sendMessage = new SendMessage();
                sendMessage.setChatId(chatId);
                List<ReqOneRowButtons> reqOneRowButtons = new ArrayList<>();
                reqOneRowButtons.add(new ReqOneRowButtons("Bosh menu", BotConstant.BACK_TO_MAIN));
                reqOneRowButtons.add(new ReqOneRowButtons("===>", BotConstant.NEW_ORDER_CLICKED));
                sendMessage.setReplyMarkup(myButtons.oneRowButtons(reqOneRowButtons));
                sendMessage.setText("Yangi buyurtma ");
                send(sendMessage);
                tgService.changeState(chatId, BotState.SHOWED_CLIENT_ORDERS);
            } else if (data.equals(BotConstant.NEW_ORDER_CLICKED)) {
                delete(chatId);
                SendMessage sendMessage = new SendMessage();
                sendMessage.setText("Xabaringizni qoldiring. (Ushbu xabarda o'zingizga kerak bo'ladigan mahsulot haqida batafsil yozishingiz mumkin)");
                sendMessage.setChatId(chatId);
                send(sendMessage);
                tgService.changeState(chatId, BotState.NEW_ORDER_DESC_ENTERING);
            }
        }
    }


    public void send(SendMessage sendMessage) {
        try {
            sendMessage.setParseMode(ParseMode.HTML);
            org.telegram.telegrambots.meta.api.objects.Message message = execute(sendMessage);
            tgMessageRepository.save(new TgMessage(Long.parseLong(sendMessage.getChatId()), message.getMessageId()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void delete(Long chatId) {
        List<TgMessage> allByChatId = tgMessageRepository.findAllByTgId(chatId);
        for (TgMessage tgMessage : allByChatId) {
            DeleteMessage deleteMessage = new DeleteMessage();
            deleteMessage.setMessageId(tgMessage.getMessageId());
            deleteMessage.setChatId(chatId);
            try {
                execute(deleteMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
        tgMessageRepository.deleteAll(allByChatId);
    }

    public void deleteClientMessage(Update req) {
        DeleteMessage deleteMessage = new DeleteMessage();
        deleteMessage.setChatId(req.getMessage().getChatId());
        deleteMessage.setMessageId(req.getMessage().getMessageId());
        try {
            execute(deleteMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
