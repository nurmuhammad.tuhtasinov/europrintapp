package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
}
