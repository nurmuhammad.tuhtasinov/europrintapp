package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.TelegramState;
import ecma.demo.europrintserver.entity.TgMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TelegramStateRepository extends JpaRepository<TelegramState, UUID> {

   Optional<TelegramState> findByTgId(Long tgId);

}
