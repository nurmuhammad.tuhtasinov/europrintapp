package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {

    Optional<Client> findByTgId(Long telegramId);

    Page<Client> findAllByFullNameContainingIgnoreCaseOrPhoneNumberContainingOrderByCreatedAtDesc(Pageable pageable,String search, String search1);

}
