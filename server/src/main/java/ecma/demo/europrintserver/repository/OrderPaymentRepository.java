package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.OrderPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderPaymentRepository extends JpaRepository<OrderPayment, UUID> {
}
