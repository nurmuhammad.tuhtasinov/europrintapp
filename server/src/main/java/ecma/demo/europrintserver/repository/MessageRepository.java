package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {

    List<Message> findAllByMessageStatusOrderByCreatedAtAsc(MessageStatus messageStatus);

}
