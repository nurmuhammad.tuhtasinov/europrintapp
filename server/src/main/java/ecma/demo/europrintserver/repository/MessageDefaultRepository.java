package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.MessageDefault;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MessageDefaultRepository extends JpaRepository<MessageDefault, UUID> {
}
