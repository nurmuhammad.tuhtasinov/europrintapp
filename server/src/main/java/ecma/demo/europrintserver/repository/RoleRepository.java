package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Payment;
import ecma.demo.europrintserver.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
}
