package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
}
