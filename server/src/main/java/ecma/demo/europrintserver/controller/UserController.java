package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.UserRepository;
import ecma.demo.europrintserver.security.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin("*")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    //    @CrossOrigin("*")
    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        if (user==null) {
            return ResponseEntity.ok(new ApiResponse("error", false));
        } else {
            return ResponseEntity.ok(new ApiResponse("success", true, user));
        }
    }

}
