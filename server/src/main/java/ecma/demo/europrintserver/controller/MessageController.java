package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.MessageRepository;
import ecma.demo.europrintserver.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @GetMapping
    public HttpEntity<?> getAll(){
        return messageService.getAll(MessageStatus.ACTIVE);
    }

    @PatchMapping("{id}")
    public HttpEntity<?> update(@PathVariable Integer id, @RequestParam String state){
        System.out.println("salom");
//       return messageService.update(id,state);
        return null;
    }

}
