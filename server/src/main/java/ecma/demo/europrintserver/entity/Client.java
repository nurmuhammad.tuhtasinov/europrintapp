package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Client")
public class Client extends AbsEntity {

    private String fullName;

    @Column(nullable = false,unique = true)
    private String phoneNumber;

    @OneToOne(fetch = LAZY)
    private TelegramState telegramState;

    @Column(unique = true)
    private Long tgId;

    public Client(String fullName, String phoneNumber) {
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
    }
}
