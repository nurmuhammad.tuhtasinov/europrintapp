package ecma.demo.europrintserver.entity.enums;

public enum  MessageStatus {

    ACTIVE,
    INACTIVE,
    PRICING,
    DESIGNING,
    CONSULTING,
    ACCEPTED,
    IGNORED

}
