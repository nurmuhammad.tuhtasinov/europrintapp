package ecma.demo.europrintserver.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsEntity {

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer amount;

//    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonManagedReference
    private Order order;

    private String productName;
    private String threeDSize;
    private String material;
    private String knife;
    private String process;
    private String type;

}
